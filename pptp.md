---
title: Wget Website Crawler
author: Mads - Eksponent
patat:
    images:
        backend: auto
    wrap: true
    margins:
        left: 2
        right: 2
    incrementalLists: true
    slideLevel: 3
    theme:
        emph: [vividBlue, onVividBlack, italic]
        strong: [bold, vividGreen]
        imageTarget: [onDullWhite, vividBlue]
        header: [bold, dullRed]

...

![](fancy.png)


# **Wget - The non-interactive network downloader.**


### Geturl
- Hrvoje Nikšić.
- For 23 år sidden blev Geturl 1.0 udgivet. (Januar 1996)
- Wget 1.4.0 (Nov. 1996) Første release med det nye navn
- GNU Wget is a free utility for non-interactive download of files from the Web.  It supports HTTP, HTTPS, and FTP protocols, as well as retrieval through HTTP proxies.
- GPL v3
- Wget v. 2.0


# Windows ?
![](graf.png)



### Wget i powershell
- Invoke-WebRequest
- Wget med ssl support: https://eternallybored.org/misc/wget/
- Remove-Item Alias:\wget
- Demo.


### Crawler vs Spider
- Samme ting ?
- En Spider Følger links på en side, og som benyttes ofte til at bygge en index eller sitemap.
- En Crawler er et general term for et program som downloader websider.


### Params der kan bruges når man laver en crawler

* --no-clobber, -nc
    > Hvis en fil er downloaded mere end engang i samme mappe, bliver den ikke downloaded igen

    > som standard laver wget en **Filename.ext.1**, **Filename.ext.2**, **Filename.ext.3**...
* --page-requisites, -p
    > Hent alle filer som er nødvendige for at html siden kan vises korrekt, dette inkludere billeder, video, lyd.

* --adjust-extension, -E
    > Checker op imod MEME type og sætter extension.
    
    > https://site.com/article?id=1234 bliver til article?id=1234.html

* --convert-links
    > Konvertere links så man faktisk kan benytte sin crawled udgave af siden.

* -nv
    > Not verbose ( giver mindre output i terminal)

* --recursive, -r 
    > Recursive

* --level [No]
    > Hvor dybt skal vi crawle ?

    > 5 er default.

* --execute, -e robots=off
    > Disable robots.txt reglerne

*  --user-agent, -U [**UserAgentString**]
    > Sæt useragent string.

* --random-wait
    > Vent imellem 0.5 => 1.5 sec imellem hvert request

    > Kan kombineres med Wait

* --wait [sec]
    > Vent XX sec imellem hvert request.


### Wget Crawler 
    wget --no-clobber \
        --convert-links \
        -nv \
        --random-wait \
        -r \
        -p \
        --level 2 \
        -E \
        -e robots=off \
        -U "IBM WebExplorer /v0.94" \
        https://www.domain.dk/

- Demo


# https://bitbucket.org/madsmoerch/wget-demo